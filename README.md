# OpenBibles
This is a node module for easily calling up English Bibles. This is originally built as a dependancy for the Electron app [Heb12 Desktop](https://github.com/heb12/heb12).

## Installation
To install the module, run `npm i openbibles`. 

## Usage
```
const bible = require('openbibles')
console.log(bible('Genesis 1:1', 'en-kjv')) // Returns "In the beginning God created the heaven and the earth."
```

*Note:* the language code on front of the translation name is not required for English due to backwards compatability. However, every other language requires language code (e.g. fr-bdc).

## Supported Translations
You can use any of these translations with `openbibles`:
- en-acv
- en-asv
- en-darby
- en-jub2000
- en-kj2000
- en-kjv
- en-nheb
- en-rhe
- en-rsv
- en-wbt
- en-web
- en-ylt
- fr-bdc

## Credits
OpenBibles Copyright (c) 2018, 2019 MasterOfTheTiger. MIT License.

Uses some code from the [Holy Bible](https://github.com/bricejlin/holy-bible/) Node module. Copyright (c) 2015 Brice Lin. MIT License.

Uses bibles from https://github.com/gratis-bible/bible in English. They have been sorted through and only ones that have the full Bible are included. Copyright (c) 2015 Matthew Cook. MIT License. The actual Bible text belongs to their respective holders (see copyright notices in Bible files).